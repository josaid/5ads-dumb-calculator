package com.josaid.dumbcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.mozilla.javascript.EcmaError;
import org.mozilla.javascript.EvaluatorException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public String getEquation() {
        return equation;
    }

    public void setEquation(String equation) {
        this.equation = equation;
        edtResult.setText(this.equation.isEmpty() ? "0" : this.equation);
    }

    public void appendEquation(String partialEquation) {
        setEquation(this.equation + partialEquation);
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
        edtResult.setText(this.current.isEmpty() ? "0" : this.current);
    }

    public void appendCurrent(String partialCurrent) {
        setCurrent(this.current + partialCurrent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int operator : allowedDigits) {
            findViewById(operator).setOnClickListener(onDigitClickListener);
        }

        for (int operator : allowedOperators) {
            findViewById(operator).setOnClickListener(onOperatorClickListener);
        }

        btnDecimal = findViewById(R.id.btnDecimal);
        btnDecimal.setOnClickListener(onDecimalClickListener);

        btnEquals = findViewById(R.id.btnEquals);
        btnEquals.setOnClickListener(onEqualsClickListener);

        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(onClearClickListener);

        edtResult = findViewById(R.id.edtResult);

        txError = findViewById(R.id.txError);

        substitutionMap.put(",", ".");
        substitutionMap.put("x", "*");
    }

    private String substitute(String string) {
        return substitutionMap.containsKey(string)
                ? substitutionMap.get(string)
                : string;
    }

    private void flushErrorIfNeeded() {
        if (lastActions.contains(CALCULATOR_ACTIONS.ERROR)) {
            btnClear.callOnClick();
        }
    }

    private String tryToSolve(String equation) {
        try {
            return equationSolver.solve(equation);
        } catch (EvaluatorException e) {
            Log.d(TAG, String.format(
                    "An error occurred when trying to solve the following equation: [%s]",
                    equation
            ));
            return null;
        } catch (EcmaError e) {
            return equation;
        }
    }

    private enum CALCULATOR_ACTIONS {
        DIGIT,
        OPERATOR,
        OPERATOR_SPECIAL,
        DECIMAL,
        CLEAR,
        EQUALS,
        ERROR
    }

    private final EquationSolver equationSolver = new EquationSolver();
    private static final String TAG = MainActivity.class.getSimpleName();

    private String equation = "",
            current = "",
            lastValidSubEquation = "";

    private final int[] allowedDigits = {
            R.id.btnZero, R.id.btnOne, R.id.btnTwo,
            R.id.btnThree, R.id.btnFour, R.id.btnFive,
            R.id.btnSix, R.id.btnSeven, R.id.btnEight, R.id.btnNine
    }, allowedOperators = {
            R.id.btnAddition, R.id.btnSubtraction, R.id.btnMultiplication,
            R.id.btnDivision, R.id.btnPercentage
    };

    private final Map<String, String> substitutionMap = new HashMap<>();

    private EditText edtResult;

    private TextView txError;

    private Button btnDecimal, btnEquals, btnClear;

    private final List<CALCULATOR_ACTIONS> lastActions = new ArrayList<>();

    View.OnClickListener onDigitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            flushErrorIfNeeded();
            for (int operator : allowedDigits) {
                if (view.getId() == operator) {
                    if (lastActions.size() > 0 &&
                            lastActions.lastIndexOf(CALCULATOR_ACTIONS.OPERATOR_SPECIAL) == lastActions.size() - 1) {
                        return;

                    } else if (lastActions.size() > 0 &&
                            lastActions.lastIndexOf(CALCULATOR_ACTIONS.EQUALS) == lastActions.size() - 1) {
                        btnClear.callOnClick();
                    }

                    if (operator == R.id.btnZero && (current.length() == 1 && current.startsWith("0"))) {
                        // Special case, no trailing zeros are allowed
                        break;
                    }
                    appendCurrent(((Button) view).getText().toString());
                    lastValidSubEquation += ((Button) view).getText().toString();
                    lastActions.add(CALCULATOR_ACTIONS.DIGIT);
                    break;
                }
            }
        }
    };

    View.OnClickListener onOperatorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            flushErrorIfNeeded();
            for (int operator : allowedOperators) {
                if (view.getId() == operator) {
                    boolean isSpecial = false;
                    if (lastActions.lastIndexOf(CALCULATOR_ACTIONS.OPERATOR) == lastActions.size() - 1) {
                        return;
                    }

                    if (operator == R.id.btnPercentage) {
                        if (getCurrent().isEmpty()) {
                            String valueSolved = tryToSolve(getEquation());
                            if (valueSolved != null)
                                setEquation(String.format("(%s / 100)", valueSolved));
                            else return;
                        } else {
                            appendEquation(String.format("(%s / 100)", getCurrent()));
                        }
                        isSpecial = true;
                    } else {
                        appendEquation(getCurrent());
                    }

                    if (lastActions.lastIndexOf(CALCULATOR_ACTIONS.OPERATOR) >= 1) {
                        setEquation(String.format("(%s)", getEquation()));
                    }


                    if (!isSpecial) {
                        String valueToSolve = getEquation();

                        appendEquation(String.format(" %s ", substitute((String) ((Button) view).getText())));

                        if (lastActions.contains(CALCULATOR_ACTIONS.OPERATOR) &&
                                lastActions.lastIndexOf(CALCULATOR_ACTIONS.EQUALS) != lastActions.size() - 1) {
                            String valueSolved = tryToSolve(valueToSolve);
                            if (valueSolved != null) edtResult.setText(valueSolved);
                        }
                    }

                    setCurrent("");

                    lastValidSubEquation = isSpecial ? "" : (substitute((String) ((Button) view).getText()) + " ");
                    lastActions.add(isSpecial ? CALCULATOR_ACTIONS.OPERATOR_SPECIAL : CALCULATOR_ACTIONS.OPERATOR);
                    break;
                }
            }
        }
    };

    View.OnClickListener onDecimalClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (lastActions.lastIndexOf(CALCULATOR_ACTIONS.ERROR) == lastActions.size() - 1) {
                return;
            }

            if (view.getId() == R.id.btnDecimal &&
                    !current.contains(".") &&
                    lastActions.size() > 0 &&
                    lastActions.get(lastActions.size() - 1) == CALCULATOR_ACTIONS.DIGIT) {
                appendCurrent(substitute((String) ((Button) view).getText()));
                lastActions.add(CALCULATOR_ACTIONS.DECIMAL);
            }
        }
    };

    View.OnClickListener onEqualsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btnEquals) {
                if (lastActions.lastIndexOf(CALCULATOR_ACTIONS.ERROR) == lastActions.size() - 1 ||
                        equation.isEmpty()) {
                    return;
                }

                String valueToSolve;
                if (lastActions.lastIndexOf(CALCULATOR_ACTIONS.EQUALS) == lastActions.size() - 1 &&
                        !lastValidSubEquation.isEmpty()) {
                    valueToSolve = String.format("%s %s", getEquation(), lastValidSubEquation);
                } else {
                    String current = getCurrent();
                    setCurrent("");
                    valueToSolve = getEquation() + current;
                }

                String valueSolved = tryToSolve(valueToSolve);
                if (valueSolved != null) {
                    setEquation(valueSolved);
                    lastActions.add(CALCULATOR_ACTIONS.EQUALS);
                } else {
                    txError.setVisibility(View.VISIBLE);
                    lastActions.add(CALCULATOR_ACTIONS.ERROR);
                }
            }
        }
    };

    View.OnClickListener onClearClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btnClear) {
                setEquation("");
                setCurrent("");
                lastValidSubEquation = "";
                txError.setVisibility(View.INVISIBLE);
                lastActions.clear();
                lastActions.add(CALCULATOR_ACTIONS.CLEAR);
            }
        }
    };
}